import Quill from 'quill';
import { VideoHandler, ImageHandler, Options } from 'ngx-quill-upload';
export const QuillConfiguration = {
  toolbar: [
    ['bold', 'italic', 'underline', 'strike'],
    ['blockquote', 'code-block'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    [{ header: [1, 2, 3, 4, 5, 6, false] }],
    [{ color: [] }, { background: [] }],
    ['link', 'image', 'attachment'],
    ['clean'],
  ],
  imageHandler: {
    upload: (file) => {
      return; // your uploaded image URL as Promise<string>
    },
    accepts: ['png', 'jpg', 'jpeg', 'jfif'], // Extensions to allow for images (Optional) | Default - ['jpg', 'jpeg', 'png']
  } as Options,
};
