import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { QuillConfiguration } from './quill-configuration';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  quillConfiguration = QuillConfiguration;
  example: FormControl;
  constructor() {}

  ngOnInit() {
    this.example = new FormControl();
  }

  onSubmit() {
    console.log('Raw Data', this.example.value);
  }
}
